package com.Controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import  org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.Service.*;
import com.bean.*;

@Controller
@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserService userservice;
	@GetMapping(value="getAllUsers",
			produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Users> getAllUserDetailsInfo() {
		return userservice.getAllUserDetails();
	}
	@PostMapping(value = "storeUsers",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUserDetailsInfo(@RequestBody Users user) {
		
				return userservice.storeUserDetailsInfo(user);
	}
	@PatchMapping(value = "updateUsers")
	public String updateUserDetailsInfo(@RequestBody Users user) {
					return userservice.updateUserDetailsInfo(user);
	}
	@DeleteMapping(value = "deleteUsers/{U_Id}")
	public String storeBooksInfo(@PathVariable("U_Id") int U_Id) {
					return userservice.deleteUserDetailsInfo(U_Id);
	}

}


