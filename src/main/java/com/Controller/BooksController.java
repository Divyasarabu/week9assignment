package com.Controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import  org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.Service.*;
import com.bean.*;

@Controller
@RestController
@RequestMapping("/books")
public class BooksController {
	@Autowired
	BooksService booksservice;
	@GetMapping(value="getAllBooks",
			produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Books> getAllBooksInfo() {
		return booksservice.getAllBooks();
	}
	@PostMapping(value = "storeBooks",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBooksInfo(@RequestBody Books book) {
		
				return booksservice.storeBooksInfo(book);
	}
	@PatchMapping(value = "updateBooks")
	public String updateBooksInfo(@RequestBody Books book) {
					return booksservice.updateBooksInfo(book);
	}
	@DeleteMapping(value = "deleteBooks/{B_Id}")
	public String storeBooksInfo(@PathVariable("B_Id") int B_Id) {
					return booksservice.deleteBooksInfo(B_Id);
	}

}
