package com.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.*;

import com.Service.*;

@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	AdminService adminservice;
	
		@PostMapping(value = "register",
				consumes = MediaType.APPLICATION_JSON_VALUE)
		public String storeAdminInfo(@RequestBody Admin admin) {
			
					return adminservice.storeAdminInfo(admin);
		}
		
		@PatchMapping(value = "updateAdminU_Name")
		public String updateAdminInfo(@RequestBody Admin admin) {
						return adminservice.updateAdminInfo(admin);
		}
		
		@PostMapping(value = "login",
				consumes = MediaType.APPLICATION_JSON_VALUE)
		public String storeAdminLoginInfo(@RequestBody Admin admin) {
			
					return adminservice.storeAdminLoginInfo(admin);
		}
		
		@GetMapping(value = "logout/{U_Name}")
		public String logout(@PathVariable("U_Name") String U_Name) {
			return adminservice.AdminlogOutInfo(U_Name);
		}

		
		
	}
		



