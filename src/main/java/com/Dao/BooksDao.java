package com.Dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bean.*;

@Repository
public interface BooksDao extends JpaRepository <Books,Integer> {

}
