package com.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.*;
import com.Dao.*;

@Service
public class UserService {
	@Autowired
	UserDao userdao;
	 public List <Users> getAllUserDetails () {
		 return userdao.findAll();
	 }

		public String storeUserDetailsInfo(Users user) {
					
							if(userdao.existsById(user.getU_Id())) {
										return "U_Id not present";
							}else {
										userdao.save(user);
										return "UserDetails Stored Successfully";
							}
		}
		
		public String deleteUserDetailsInfo(int U_Id) {
			if(!userdao.existsById(U_Id)) {
				return "User is  not present";
				}else {
				userdao.deleteById(U_Id);
				return "UserDetails Deleted Successfully";
				}	
		}
		
		public String updateUserDetailsInfo(Users user) {
			if(!userdao.existsById(user.getU_Id())) {
				return "User Details not present";
				}else {
				Users  q = userdao.getById(user.getU_Id());	// if product not present it will give exception 
				q.setU_Name(user.getU_Name());						// existing product price change 
				userdao.saveAndFlush(user);				// save and flush method to update the existing product
				return "User Details Updated Successfully";
				}	
		}
		
	}



