package com.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;

import com.Dao.*;

@Service
public class AdminService {
@Autowired
AdminDao admindao;
public String storeAdminInfo(Admin admin) {
	
	if(admindao.existsById(admin.getU_Name())) {
				return "Admin U_Name must be unique";
	}else {
				admindao.save(admin);
				return "Admin U_Name  Stored Successfully";
	        }
      }

   
public String updateAdminInfo(Admin admin) {
if(!admindao.existsById(admin.getU_Name())) {
	return " Details Are wrong";
	}else  {
	Admin x	= admindao.getById(admin.getU_Name());
	x.setPassword(admin.getPassword());
	x.setU_Name(admin.getU_Name());
	admindao.saveAndFlush(x);
	return "Admin Details Are  Updated ";
	}	
}
public String storeAdminLoginInfo(Admin admin) {
	
	if(!admindao.existsById(admin.getU_Name())) {
				return "U_Name must be unique";
	}else {
				admindao.save(admin);
				return "login  successfully";
	        }
      }
public String AdminlogOutInfo(String U_Name) {
	if(admindao.existsById(U_Name)) {
		return " LogOut Successfully";
	}
	else {
		return "Failed";
	}


}
}
