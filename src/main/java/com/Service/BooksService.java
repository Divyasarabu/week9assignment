package com.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Books;
import com.Dao.BooksDao;

@Service
public class BooksService {
	@Autowired
	BooksDao booksdao;
	 public List <Books> getAllBooks () {
		 return booksdao.findAll();
	 }

		public String storeBooksInfo(Books book) {
					
							if(booksdao.existsById(book.getB_Id())) {
										return "B_Id not stored";
							}else {
										booksdao.save(book);
										return "Book Stored Successfully";
							}
		}
		
		public String deleteBooksInfo(int B_Id) {
			if(!booksdao.existsById(B_Id)) {
				return "Book is  not present";
				}else {
				booksdao.deleteById(B_Id);
				return "Book Deleted Successfully";
				}	
		}
		
		public String updateBooksInfo(Books book) {
			if(!booksdao.existsById(book.getB_Id())) {
				return "book details not present";
				}else {
				Books p	= booksdao.getById(book.getB_Id());	
				p.setB_Rating(book.getB_Rating());	
				p.setB_Name(book.getB_Name());
				p.setB_Id(book.getB_Id());
				booksdao.saveAndFlush(p);				// save and flush method to update the existing product
				return "Book Details Updated Successfully";
				}	
		}
		
	}
