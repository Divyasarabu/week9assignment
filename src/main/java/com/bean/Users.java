package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Users {
	@Id
    private int U_Id;
	private String U_Name;
	private String Password;
	
	public int getU_Id() {
		return U_Id;
	}
	public void setU_Id(int U_Id) {
		this.U_Id = U_Id;
	}
	
	public String getU_Name() {
		return U_Name;
	}
	public void setU_Name(String U_Name) {
		this.U_Name = U_Name;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String Password) {
		this.Password = Password;
	}
	

	
	
}

