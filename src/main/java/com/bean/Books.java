package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Books{
	@Id

private int B_Id;
private String B_Name;
private float B_Rating;
public int getB_Id() {
	return B_Id;
}
public void setB_Id(int B_Id) {
	this.B_Id = B_Id;
}
public String getB_Name() {
	return B_Name;
}
public void setB_Name(String B_Name) {
	this.B_Name = B_Name;
}
public float getB_Rating() {
	return B_Rating;
}
public void setB_Rating(float B_Rating) {
	this.B_Rating = B_Rating;
}


}



