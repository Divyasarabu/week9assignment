package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {
	@Id
	private String U_Name;
	private String Password;
	public String getU_Name() {
		return U_Name;
	}
	public void setU_Name(String U_Name) {
		this.U_Name = U_Name;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		this.Password = password;
	}
	

}
